Implementazione del classico gioco del FizzBuzz, in cui viene dato in input un numero e si controlla la sua divisibilità per 3 e 5, ritornando una stringa contenente "fizz" se il numero è divisibile per 3, "buzz" se divisibile per 5 o "fizzbuzz" se per entrambi. Ciò dovrà essere fatto per ogni numero da 1 a N.

Nel complesso, l'esperienza è stata utile e formativa, soprattutto per avvicinarsi all'utilizzo dei test.
Inoltre è stata molto utile anche la parte su git, dovendo capire come funzionassero i merge tra vari branch.
Raccomandandola, dò come voto un 4/5.
