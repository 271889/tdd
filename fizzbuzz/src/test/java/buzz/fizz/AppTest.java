package buzz.fizz;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void fizzBuzzTest()
    {
        assertEquals("1","1", App.fizzBuzz(1));
    }

    @Test
    public void fizzBuzzTest2()
    {
        assertEquals("3","12fizz", App.fizzBuzz(3));
    }

    @Test
    public void fizzBuzzTest3()
    {
        assertEquals("5","12fizz4buzz", App.fizzBuzz(5));
    }

    @Test
    public void fizzBuzzTest4()
    {
        assertEquals("15","12fizz4buzzfizz78fizzbuzz11fizz1314fizzbuzz", App.fizzBuzz(15));
    }
}
