package buzz.fizz;

/**
 * Hello world!
 *
 */
public class App 
{

   public static String fizzBuzz(int N){
       
    String s = "";
    for(int i=1; i<=N; i++) {

        if(i%3==0)
            s+="fizz";
        if(i%5==0)
            s+="buzz";
        if(i%3!=0 && i%5!=0)
            s += i;
    }
    return s;
    }
}